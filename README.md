# [CRE] Automated Testing Basics in Java learining program home tasks repository

## Information about project directory tree and files in it

```
.\
│ pom.xml
│
└─src\
  ├─main\
  │ ├─java\com\epam\training\student_volodymyr_poddubetskyi\webdriver\
  │ │ ├─core\
  │ │ │   DriverSingleton.java
  │ │ │   TestListener.java
  │ │ │
  │ │ ├─model\
  │ │ │   EsimateMailMessageAddressee.java
  │ │ │   EsimateMailMessageAddresseeCreator.java
  │ │ │
  │ │ ├─pageobjects\
  │ │ │ │ AbstractPage.java
  │ │ │ │
  │ │ │ ├─cloud_google_com\
  │ │ │ │ │ CloudGoogleComHomePage.java
  │ │ │ │ │ SearchResultTransitPage.java
  │ │ │ │ │
  │ │ │ │ ├─gcp_pricing_calculator\
  │ │ │ │ │   AbstractExpectedEmailMessage.java
  │ │ │ │ │   GCPPricingCalculatorApp.java
  │ │ │ │ │   GCPPricingCalculatorAppComputeEnginePanel.java
  │ │ │ │ │   GCPPricingCalculatorEstimateMailMessageBody.java
  │ │ │ │ │   GCPPricingCalculatorPage.java
  │ │ │ │ │
  │ │ │ │ └─search\
  │ │ │ │     GCPSearchResultsPage.java
  │ │ │ │
  │ │ │ ├─snippethost\
  │ │ │ │   SnippetHostHomePage.java
  │ │ │ │   SnippetHostSnippetPublicationResultPage.java
  │ │ │ │
  │ │ │ └─yopmail_com\
  │ │ │     YopmailComGeneratedMailBoxInboxPage.java
  │ │ │     YopmailComHomePage.java
  │ │ │     YopmailComInboxMessageEntriesListPane.java
  │ │ │     YopmailComRandomMailBoxAddressGenerationResultPage.java
  │ │ │
  │ │ └─service\
  │ │    TemporaryMailServiceClient.java
  │ │    TestDataReader.java
  │ │
  │ └─resources\
  │     dev.properties
  │     qa.properties
  │     log4j2.xml
  │
  └─test\
    ├─java\com\epam\training\student_volodymyr_poddubetskyi\webdriver\
    │ │  CommonConditions.java
    │ │  
    │ ├─task01\
    │ │   SnippetHostContentCreationTaskTests.java
    │ │
    │ ├─task02\
    │ │   SnippetHostMultilineContentCreationTaskTests.java
    │ │
    │ └─task03\
    │     CheckTheComputeEngineCalculationInEmailMatchesTheSiteTests.java
    │
    └─resources\
        testng-all-gcp.xml
        testng-all-sites.xml
        testng-smoke.xml

```

## Webdriver tasks solutions with TA Framework components added

1) Common path for all the code files of the webdriver tasks, that don't contain tests:  
   `.\src\main\java\com\epam\training\student_volodymyr_poddubetskyi\webdriver\`
   1) `core\`
      1) `DriverSingleton.java` - A WebDriver manager for managing browser connections
      2) `TestListener.java` - Test listener class code to take screenshots in case of a test failure
   2) `model`
      1) `EsimateMailMessageAddressee.java` - a model to supply the «Email Your Estimate» form with data
      2) `EsimateMailMessageAddresseeCreator.java` - a helper code to prepare different `EsimateMailMessageAddressee` instances
   3) `pageobjects\` - a directory that contains the code of the `pageobject` classes:
      1) `AbstractPage.java` - An abstract page class code
      2) `cloud_google_com\` - PageObject model classes of the `cloud.google.com` website
         1) `CloudGoogleComHomePage.java` - the home page
         2) `SearchResultTransitPage.java` - transit page that contains a link to GCP pricing calculator page
         3) `gcp_pricing_calculator\` - GCP pricing calculator web application related 
            1) `AbstractExpectedEmailMessage.java` - the file that contains abstract email message interface declaration
            2) `GCPPricingCalculatorApp.java` - PageObject model classes that are related to GCP pricing calculator web application page
            3) `GCPPricingCalculatorAppComputeEnginePanel.java` - GCP pricing calculator compute engine calculations view
            4) `GCPPricingCalculatorEstimateMailMessageBody.java` - GCP pricing calculator estimate email message body
            5) `GCPPricingCalculatorPage.java` - the page that contains GCP pricing calculator web application
         4) `search\` - PageObject model classes that are related to GCP search functionality
            1) `GCPSearchResultsPage.java` - search results page
      3) `snippethost\` - PageObject model classes of the `snippet.host` website
         1) `SnippetHostHomePage.java` - the home page
         2) `SnippetHostSnippetPublicationResultPage.java` - content publication result page
      4) `yopmail_com\` - PageObject model classes of the `yopmail.com` website
         1) `YopmailComGeneratedMailBoxInboxPage.java` - temporary mailbox inbox page
         2) `YopmailComHomePage.java` - the home page
         3) `YopmailComInboxMessageEntriesListPane.java` - message entries list on the inbox page
         4) `YopmailComRandomMailBoxAddressGenerationResultPage.java` - the page that contains just generated email address value
   4) `service\` - a directory containing code files that are implementations of the services used to meet the needs caused by the testing conditions
      1) `TemporaryMailServiceClient.java` - a file containing the code that enables the use of the yopmail.com temporary mailbox service for testing purposes
      2) `TestDataReader.java`
2) Common path for all the code files that contain tests:  
   `.\src\test\java\com\epam\training\student_volodymyr_poddubetskyi\webdriver\`
   1) `task01\` - The directory, that contains the code of a solution for the task 1
      1) `SnippetHostContentCreationTaskTests.java` - this file contains the code of a class, that is an implementation of a solution for the task 1.
   2) `task02\` - The directory, that contains the code of a solution for the task 2
      1) `SnippetHostMultilineContentCreationTaskTests.java` - this file contains the code of a class, that is an implementation of a solution for the task 2.
   3) `task03\` - The directory, that contains the code of a solution for the task 3
      1) `CheckTheComputeEngineCalculationInEmailMatchesTheSiteTests.java` - this file contains the code of a class, that is an implementation of a solution for the task 3.

3) Resources:
   1) `.\src\test\resources`
      1) `dev.properties` - «development environment» data 
      2) `qa.properties` - «Quality Assurance environment» data
      3) `log4j2.xml` - log4j2 logger setup parameters file
   2) `.\src\test\resources`
      1) `testng-all-gcp.xml` - TestNG test suite that includes webdriver task 3 test as a reference to its test package
      2) `testng-smoke.xml` - TestNG test suite that includes webdriver task 3 test as a reference to its test class
      3) `testng-all-sites.xml` - TestNG test suite that includes webdriver tasks 1-3 tests as references to their test packages

4) Logging. Logs are saved into `.\target\logs`.
5) Screenshots capturing. In case of test failures screenshots are saved into `.\target\screenshots`.

## Running test suites on environments

By default, it is possible to run each test class separately. It will run with the **`dev`** environment settings and in **Chrome browser**. To run test class of particular task the following commands can be used:
1) Running test class of the webdriver task 1:
   This solution can be run from CLI using the following commnd:
   ```cmd
   mvn test --define test="SnippetHostContentCreationTaskTests"
   ```
2) Running test class of the webdriver task 2:

   ```cmd
   mvn test --define test="SnippetHostMultilineContentCreationTaskTests"
   ```
3) Running test class of the webdriver task 3:
   ```cmd
   mvn test --define test="CheckTheComputeEngineCalculationInEmailMatchesTheSiteTests"
   ```

Other options:
1) Running smoke test using **`dev`** environment settings and **Mozilla Firefox** browser:
   ```
   mvn test --define browser=firefox --define environment=dev --define surefire.suiteXmlFiles=src\test\resources\testng-smoke.xml
   ```
2) Running all webdriver task 3 tests using **`dev`** environment settings and **Mozilla Firefox** browser:
   ```
   mvn test --define browser=firefox --define environment=dev --define surefire.suiteXmlFiles=src\test\resources\testng-all-gcp.xml
   ```
3) Running all webdriver tasks tests using **`dev`** environment settings and **Mozilla Firefox** browser:
   ```
   mvn test --define browser=firefox --define environment=dev --define surefire.suiteXmlFiles=src\test\resources\testng-all-sites.xml
   ```
4) Running all webdriver tasks tests using **`qa`** environment settings and **Google Chrome** browser:
   ```
   mvn test --define browser=chrome --define environment=qa --define surefire.suiteXmlFiles=src\test\resources\testng-all-sites.xml
   ```

