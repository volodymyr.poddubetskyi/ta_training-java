package com.epam.training.student_volodymyr_poddubetskyi
          .webdriver.task01;

import com.epam.training.student_volodymyr_poddubetskyi
         .webdriver.CommonConditions;
import com.epam.training.student_volodymyr_poddubetskyi
         .webdriver.pageobjects.snippethost.SnippetHostHomePage;

import static org.testng.Assert.assertTrue;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.annotations.Test;

/**
 * WebDriver module task 1 solution implementation
 */
public class SnippetHostContentCreationTaskTests extends CommonConditions {

    private static final String CONTENT_TO_BE_PUBLISHED="Hello from WebDriver";
    private static final String CONTENT_TITLE="helloweb";

    private final Logger log = LogManager.getRootLogger();

    @Test
    public void snippetHostContentCreationTaskTest()
    {
      int actualNumberOfSignsOfSuccessfulPublication  =
            new SnippetHostHomePage(driver)
                  .openPage()
                  .publishASnippetWithTitleAndExpiryTimeIn10Minutes(
                    CONTENT_TITLE,
                    CONTENT_TO_BE_PUBLISHED)
                  .countTheNumberOfSignsOfSuccessfulPublication();

      assertTrue(actualNumberOfSignsOfSuccessfulPublication==1,
        "The actual count of signs of successful publication is differ from expected");
      log.info("Published content page url is as follows: " + driver.getCurrentUrl());
    }
}
