package com.epam.training.student_volodymyr_poddubetskyi
          .webdriver.task03;

import com.epam.training.student_volodymyr_poddubetskyi
         .webdriver.CommonConditions;

import com.epam.training.student_volodymyr_poddubetskyi
         .webdriver.pageobjects.cloud_google_com.CloudGoogleComHomePage;
import com.epam.training.student_volodymyr_poddubetskyi
         .webdriver.pageobjects.cloud_google_com
           .gcp_pricing_calculator.AbstractExpectedEmailMessage;
import com.epam.training.student_volodymyr_poddubetskyi
         .webdriver.pageobjects.cloud_google_com
           .gcp_pricing_calculator.GCPPricingCalculatorApp;
import com.epam.training.student_volodymyr_poddubetskyi
         .webdriver.pageobjects.cloud_google_com
           .gcp_pricing_calculator.GCPPricingCalculatorEstimateMailMessageBody;
import com.epam.training.student_volodymyr_poddubetskyi
         .webdriver.service.TemporaryMailServiceClient;

import com.epam.training.student_volodymyr_poddubetskyi
         .webdriver.model.EsimateMailMessageAddressee;

import com.epam.training.student_volodymyr_poddubetskyi
         .webdriver.model.EsimateMailMessageAddresseeCreator;

import static org.testng.Assert.assertTrue;

import org.testng.annotations.Test;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class CheckTheComputeEngineCalculationInEmailMatchesTheSiteTests extends CommonConditions {

  private final String TERM_TO_SEARCH="Google Cloud Platform Pricing Calculator";

  private final Logger log = LogManager.getRootLogger();

  @Test
  public void checkTheComputeEngineCalculationInEmailMatchesTheSiteTest(){
    EsimateMailMessageAddressee mailAddressee;
    TemporaryMailServiceClient mailClient = new TemporaryMailServiceClient(driver);
    mailAddressee = EsimateMailMessageAddresseeCreator
                      .getMinimallySufficientAddressee(
                         mailClient.getNewRandomMailBoxAddress());

    log.info(mailAddressee.toString());

    GCPPricingCalculatorApp pricingCalculatorApp = new CloudGoogleComHomePage(driver)
          .openPage()
          .searchForTerm(TERM_TO_SEARCH)
          .clickTheFirstExactlyMatchedSearchResultLink(TERM_TO_SEARCH)
          .navigateALinkThatContainsSearchTermAsText(TERM_TO_SEARCH)
          .proceedToGCPPricingCalculatorApp();

    GCPPricingCalculatorEstimateMailMessageBody expectedMail = 
          pricingCalculatorApp.displayComputeEnginePanel()
            .fillOutComputeEngineCalculationParametersFormAndAddToEstimate()
            .emailTotalEstimate(mailAddressee); /* cannot use mailClient.getNewRandomMailBoxAddress()
            call here because of the hecking pain of frames and swithcing
            between them */
          
    String totalEstimate = pricingCalculatorApp.getTotalEstimate();

    AbstractExpectedEmailMessage arrivedMessage =
      mailClient.waitForEmailMessageArrivalAndReturnIt(expectedMail);

    log.info(expectedMail.getActualMailMessageAddressee().toString());
    log.info(String.format("TotalEstimate from webpage: %s", totalEstimate));
    log.info(String.format("TotalEstimate from email: %s",
                      arrivedMessage.getActualMessageContent()));


    assertTrue(arrivedMessage.getActualMessageContent().equals(totalEstimate),
               "Values of total estimate per month don't match");
  }
}
