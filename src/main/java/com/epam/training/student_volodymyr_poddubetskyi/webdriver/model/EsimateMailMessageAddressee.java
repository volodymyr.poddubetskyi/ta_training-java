package com.epam.training.student_volodymyr_poddubetskyi.webdriver.model;

import java.util.Objects;

public class EsimateMailMessageAddressee {
  private String firstName="";
  private String lastName="";
  private String email="";
  private String projectName="";
  private String projectUrl="";
  private String phone="";
  private String title="";
  private String company="";

  public EsimateMailMessageAddressee (String email) {
/*  Looks like it could be a negative test case
     if (email == null || email.isBlank()){
      throw new UnsupportedOperationException("Cannot use empty email to send Estimate Email Message");
    }
 */ 
    this.email = email;
  }

  public String getFirstName() { return firstName; }
  public void setFirstName(String firstName) { this.firstName = firstName; }

  public String getLastName() { return lastName; }
  public void setLastName(String lastName) { this.lastName = lastName; }

  public String getEmail() { return email; }
  public void setEmail(String email) { this.email = email; }

  public String getProjectName() { return projectName;}
  public void setProjectName(String projectName) { this.projectName = projectName; }

  public String getProjectUrl() { return projectUrl; }
  public void setProjectUrl(String projectUrl) { this.projectUrl = projectUrl; }

  public String getPhone() { return phone; }
  public void setPhone(String phone) { this.phone = phone; }

  public String getTitle() { return title; }
  public void setTitle(String title) { this.title = title; }

  public String getCompany() {return company;}
  public void setCompany(String company) {this.company = company;}

  @Override
  public String toString() {
    return String.format("EsimateMailMessaageAddressee{First "+
                         "Name='%s', Last Name='%s', Email='%s', "+
                         "ProjectName='%s', Project Url='%s', "+
                         "Phone='%s', Title='%s', Company='%s'}",
                          getFirstName(),
                          getLastName(),
                          getEmail(),
                          getProjectName(),
                          getProjectUrl(),
                          getPhone(),
                          getTitle(),
                          getCompany());
  }

  @Override
  public boolean equals(Object o){
    if (this==o) return true;
    if (!(o instanceof EsimateMailMessageAddressee)) return false;
    EsimateMailMessageAddressee other = (EsimateMailMessageAddressee) o;
    return Objects.equals(getFirstName(), other.getFirstName()) &&
           Objects.equals(getLastName(), other.getLastName()) &&
           Objects.equals(getEmail(), other.getEmail()) &&
           Objects.equals(getProjectName(), other.getProjectName()) &&
           Objects.equals(getProjectUrl(), other.getProjectUrl()) &&
           Objects.equals(getPhone(), other.getPhone()) &&
           Objects.equals(getTitle(), other.getTitle()) &&
           Objects.equals(getCompany(), other.getCompany());
  }

  @Override
  public int hashCode(){
    return Objects.hash(getFirstName(),
                        getLastName(),
                        getEmail(),
                        getProjectName(),
                        getProjectUrl(),
                        getPhone(),
                        getTitle(),
                        getCompany());
  }
}
