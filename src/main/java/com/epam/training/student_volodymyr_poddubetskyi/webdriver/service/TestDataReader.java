package com.epam.training.student_volodymyr_poddubetskyi.webdriver.service;

import java.util.ResourceBundle;


public class TestDataReader {

    private static ResourceBundle resourceBundle = 
      ResourceBundle.getBundle(System.getProperty("environment", "dev"));
  
    public static String getTestData(String key){
      return resourceBundle.getString(key);
    }
}
