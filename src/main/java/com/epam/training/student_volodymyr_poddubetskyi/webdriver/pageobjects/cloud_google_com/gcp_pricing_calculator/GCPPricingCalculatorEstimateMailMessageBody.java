package com.epam.training.student_volodymyr_poddubetskyi
          .webdriver.pageobjects.cloud_google_com.gcp_pricing_calculator;

import com.epam.training.student_volodymyr_poddubetskyi
         .webdriver.pageobjects.AbstractPage;

import com.epam.training.student_volodymyr_poddubetskyi
         .webdriver.model.EsimateMailMessageAddressee;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class GCPPricingCalculatorEstimateMailMessageBody
  extends AbstractPage
  implements AbstractExpectedEmailMessage{

  private String expectedMessageSubject="Google Cloud Price Estimate";
  private EsimateMailMessageAddressee mailAddressee;
  private String actualEstimatedMonthlyCostValue=null;

  /* //h2[text()[contains(.,'Estimated Monthly Cost:')]] */
  By estimatedMonthlyCostTextStringElementLocator = 
        By.xpath("//h2[text()[contains(.,'Estimated Monthly Cost:')]]");
  WebElement estimatedMonthlyCostTextStringElement;

  public GCPPricingCalculatorEstimateMailMessageBody(WebDriver driver,
                                                     EsimateMailMessageAddressee addressee) {
    super(driver);
    this.mailAddressee = addressee;
  }

  public String getEstimatedMonthlyCostValue(){
    Pattern estimatedMonthlyCostValueRegex = Pattern.compile("([0-9][0-9.,`'_ ]{2,})");
    Matcher estimatedMonthlyCostValueMatcher;

    estimatedMonthlyCostTextStringElement = 
      fluentlyWaitForSingleElement(estimatedMonthlyCostTextStringElementLocator);

    estimatedMonthlyCostValueMatcher = estimatedMonthlyCostValueRegex
                                  .matcher(estimatedMonthlyCostTextStringElement.getText());

    if (estimatedMonthlyCostValueMatcher.find()){
      this.actualEstimatedMonthlyCostValue = estimatedMonthlyCostValueMatcher.group();
    }
    return this.actualEstimatedMonthlyCostValue;
  }

  @Override
  protected AbstractPage openPage() {
    throw new UnsupportedOperationException("This method is not intended to be used");
  }

  @Override
  public String getExpectedMessageSubject() {
    return this.expectedMessageSubject;
  }

  @Override
  public EsimateMailMessageAddressee getActualMailMessageAddressee() {
    return this.mailAddressee;
  }

  @Override
  public String getActualMessageContent() {
    return this.actualEstimatedMonthlyCostValue;
  }

  @Override
  public AbstractExpectedEmailMessage doActionsToObtainActualMessageContent() {
    getEstimatedMonthlyCostValue();
    return this;
  }

}