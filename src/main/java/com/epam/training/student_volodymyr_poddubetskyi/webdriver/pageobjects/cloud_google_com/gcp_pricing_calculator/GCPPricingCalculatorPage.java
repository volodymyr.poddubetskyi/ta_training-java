package com.epam.training.student_volodymyr_poddubetskyi
          .webdriver.pageobjects.cloud_google_com.gcp_pricing_calculator;

import com.epam.training.student_volodymyr_poddubetskyi
         .webdriver.pageobjects.AbstractPage;

import java.time.Duration;

import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class GCPPricingCalculatorPage extends AbstractPage {

  private WebDriver pricingCalculatorFrameDriver;

  @FindBy(xpath = "//iframe[@src[contains(.,'calculator')]]")
  WebElement pricingCalculatorFrameElement;

  public GCPPricingCalculatorPage(WebDriver driver) {
    super(driver);
  }

  /* https://stackoverflow.com/a/76368901 */
  private WebDriver switchToPriceCalculatorFrame(WebDriver aDriver) {
    WebDriver newDriver=aDriver;
    WebDriverWait aWait = new WebDriverWait (aDriver,Duration.ofSeconds(3));
    try {
      if (aDriver.getCurrentUrl() == this.driver.getCurrentUrl()) {
         aWait = new WebDriverWait (aDriver,Duration.ofSeconds(WAIT_TIMEOUT_SECONDS));
      }
      newDriver=aWait.until(
          ExpectedConditions.frameToBeAvailableAndSwitchToIt(pricingCalculatorFrameElement));
    } catch (TimeoutException e) {
      return aDriver;
    }
    return switchToPriceCalculatorFrame(newDriver);
  }

  public GCPPricingCalculatorApp
    proceedToGCPPricingCalculatorApp(){
      pricingCalculatorFrameDriver = switchToPriceCalculatorFrame(this.driver);
      return new GCPPricingCalculatorApp(pricingCalculatorFrameDriver);
  }

  @Override
  protected AbstractPage openPage() {
    throw new UnsupportedOperationException("This method is not intended to be used");
  }
  
}
