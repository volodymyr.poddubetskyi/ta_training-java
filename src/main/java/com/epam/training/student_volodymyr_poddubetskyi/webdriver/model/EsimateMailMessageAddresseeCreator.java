package com.epam.training.student_volodymyr_poddubetskyi.webdriver.model;

import com.epam.training.student_volodymyr_poddubetskyi
         .webdriver.model.EsimateMailMessageAddressee;

public class EsimateMailMessageAddresseeCreator {

  public static EsimateMailMessageAddressee
    getMinimallySufficientAddressee(String email){
    return new EsimateMailMessageAddressee(email);
  }
}
