package com.epam.training.student_volodymyr_poddubetskyi
          .webdriver.pageobjects.cloud_google_com;

import com.epam.training.student_volodymyr_poddubetskyi
         .webdriver.pageobjects.AbstractPage;
import com.epam.training.student_volodymyr_poddubetskyi
         .webdriver.pageobjects.cloud_google_com
           .gcp_pricing_calculator.GCPPricingCalculatorPage;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class SearchResultTransitPage extends AbstractPage{
  private final String
    aLinkThatContainsSearchTermAsTextLocatorTemplate="//a[text()[contains(.,'%s')]]";

  private WebElement aLinkThatContainsSearchTermAsText;

  public SearchResultTransitPage(WebDriver driver) {
    super(driver);
  }

  public GCPPricingCalculatorPage 
    navigateALinkThatContainsSearchTermAsText (String searchTerm) {

/*       String.format(aLinkThatContainsSearchTermAsTextLocatorTemplate,
                    searchTerm); */

      aLinkThatContainsSearchTermAsText =
        fluentlyWaitForSingleElement(
          By.xpath(
            String.format(
              aLinkThatContainsSearchTermAsTextLocatorTemplate,
              searchTerm)));

      aLinkThatContainsSearchTermAsText.click();

      return new GCPPricingCalculatorPage(driver);
  }

  @Override
  protected AbstractPage openPage() {
    throw new UnsupportedOperationException("This method is not intended to be used");
  }
  
}
