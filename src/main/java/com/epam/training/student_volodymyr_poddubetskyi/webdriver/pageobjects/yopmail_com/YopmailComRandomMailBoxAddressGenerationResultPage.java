package com.epam.training.student_volodymyr_poddubetskyi
          .webdriver.pageobjects.yopmail_com;

import com.epam.training.student_volodymyr_poddubetskyi
         .webdriver.pageobjects.AbstractPage;

import java.time.Duration;
import java.util.Arrays;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class YopmailComRandomMailBoxAddressGenerationResultPage extends AbstractPage {

  private final String AD_URL_FRAGMENT = "#google_vignette";

  /* generated email on email generator page */
   @FindBy(xpath="//div[@id='egen']/div[@id='geny']")
   WebElement generatedEmailValueArea;

   /* checkInboxButton */
   @FindBy(xpath="//div[@id='egen']/following-sibling::div[@class='nw']"+
                "/button[@onclick[contains(.,'egengo')]]")
   WebElement checkInboxButton;

  @FindBy(xpath="//ins[@data-adsbygoogle-status and @aria-hidden='false']//iframe")
  WebElement fullPageAdAreaAndFrame;
  
  @FindBy(xpath="//iframe[@id='ad_iframe']")
  WebElement childIframeWithAdvertisement;

  @FindBy(xpath="//*[@id='dismiss-button']")
  WebElement adDismissButton;

  @FindBy(xpath="//*[contains(text(),'Complete the CAPTCHA to continue')]")
  WebElement capthcaProcessingRequest;


  private boolean tryClosingAd(WebDriverWait aWait){
    try {
      aWait.until(ExpectedConditions.visibilityOf(adDismissButton));
      adDismissButton.click();
    } catch (NoSuchElementException | TimeoutException e) {
      return false;
    }
    return true;
  }
  private void getRidOfGoogle_vignetteFullPageAd(){
    WebDriverWait aWait = new WebDriverWait(driver,
                            Duration.ofSeconds(5),
                            Duration.ofSeconds(1));
    try {
      aWait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(fullPageAdAreaAndFrame));
      if (!tryClosingAd(aWait)){
        aWait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(childIframeWithAdvertisement));
        tryClosingAd(aWait);
      }
    } catch (NoSuchElementException | TimeoutException e) {
      driver.switchTo().defaultContent();
      return;
    }
    driver.switchTo().defaultContent();
  }

  public YopmailComRandomMailBoxAddressGenerationResultPage(WebDriver driver) {
    super(driver);
  }

  public String getGeneratedMailBoxAddress(){
    if (driver.getCurrentUrl().indexOf(AD_URL_FRAGMENT)>0){
      getRidOfGoogle_vignetteFullPageAd();
    }
    waitForPageElementsToBeVisible(Arrays.asList(generatedEmailValueArea));
    return generatedEmailValueArea.getText();
  }

  public YopmailComGeneratedMailBoxInboxPage openGeneratedMailBoxInbox(){
    waitForElementToBeClickableAndClickIt(driver, checkInboxButton);
    return new YopmailComGeneratedMailBoxInboxPage(driver);
  }

  @Override
  protected AbstractPage openPage() {
    throw new UnsupportedOperationException("This method is not intended to be used");
  }
  
}
