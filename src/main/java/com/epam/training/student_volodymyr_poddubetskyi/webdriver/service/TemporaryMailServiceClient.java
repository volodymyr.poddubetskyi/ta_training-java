package com.epam.training.student_volodymyr_poddubetskyi.webdriver.service;

import com.epam.training.student_volodymyr_poddubetskyi
         .webdriver.pageobjects.cloud_google_com
           .gcp_pricing_calculator.AbstractExpectedEmailMessage;
import com.epam.training.student_volodymyr_poddubetskyi
         .webdriver.pageobjects.yopmail_com.YopmailComGeneratedMailBoxInboxPage;
import com.epam.training.student_volodymyr_poddubetskyi
         .webdriver.pageobjects.yopmail_com.YopmailComHomePage;
import com.epam.training.student_volodymyr_poddubetskyi
         .webdriver.pageobjects.yopmail_com
           .YopmailComRandomMailBoxAddressGenerationResultPage;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WindowType;

public class TemporaryMailServiceClient {

  private WebDriver driver;
  private String mailServiceClientWindowHandle=null;
  private String inboundWindowHandle=null;
  private String lastGeneratedMailBoxAddress=null;
  private YopmailComGeneratedMailBoxInboxPage mailboxInboxPage=null;

  public TemporaryMailServiceClient(WebDriver driver) {
    this.driver = driver;
    this.inboundWindowHandle = driver.getWindowHandle();
  }

  private WebDriver switchToMailServiceTab(){
    if (mailServiceClientWindowHandle == null) {
      this.driver.switchTo().newWindow(WindowType.TAB);
      this.mailServiceClientWindowHandle = driver.getWindowHandle();
    } else {
      this.driver.switchTo().window(mailServiceClientWindowHandle);
    }
    return this.driver;
  }

  private WebDriver returnToInboundTab(){
    if (! this.driver.getWindowHandle().equals(inboundWindowHandle)){
      this.driver.switchTo().window(inboundWindowHandle);
    }
    return this.driver;
  }

  public String getNewRandomMailBoxAddress(){
    
    YopmailComRandomMailBoxAddressGenerationResultPage
      generationResultPage = new YopmailComHomePage(switchToMailServiceTab())
              .openPage()
              .generateRandomMailBoxAddress();

    lastGeneratedMailBoxAddress = generationResultPage.getGeneratedMailBoxAddress();

    mailboxInboxPage = generationResultPage.openGeneratedMailBoxInbox()
      .clearInbox();

    returnToInboundTab();
    return lastGeneratedMailBoxAddress;
  }

  public YopmailComGeneratedMailBoxInboxPage openExistingMailBoxByItsAddress(String address){
    mailboxInboxPage = new YopmailComHomePage(switchToMailServiceTab())
              .openPage()
              .openExistingMailBoxInbox(address);
    return mailboxInboxPage;
  }

  public AbstractExpectedEmailMessage 
    waitForEmailMessageArrivalAndReturnIt (AbstractExpectedEmailMessage expectedMessage){

    if (mailboxInboxPage == null){
      mailboxInboxPage = openExistingMailBoxByItsAddress(
                           expectedMessage.getActualMailMessageAddressee().getEmail());
    }

    switchToMailServiceTab();

    mailboxInboxPage.waitForEmailMessageArrivalAndReturnIt (expectedMessage);

    returnToInboundTab();
    return expectedMessage;
  }

}
